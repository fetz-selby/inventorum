import React, { Component } from 'react';
import {HashRouter as Router, Route} from 'react-router-dom';
import {connect} from 'react-redux';
import SideBar from './components/sidebar/SideBar';
import AppBar from './components/appbar/AppBar';
import ContactsContainer from './containers/contacts/ContactsContainer';
import * as appRoute from './store/actions/appRoute';
import * as appAction from './store/actions/appActionCreators';
import './assets/styles/layout.css';
import './assets/styles/reset.css';

class App extends Component {

  menuEventHandler = (val) => {
    switch(val){
      case appRoute.CONTACTS.module:{
        this.initApp(appRoute.CONTACTS.module, appRoute.CONTACTS.path);
        break;
      }
      case appRoute.POS.module:{
        this.initApp(appRoute.POS.module, appRoute.POS.path);
        break;
      }
      case appRoute.INVOICE_RETURNS.module:{
        this.initApp(appRoute.INVOICE_RETURNS.module, appRoute.INVOICE_RETURNS.path);
        break;
      }
      case appRoute.INVENTORY.module:{
        this.initApp(appRoute.INVENTORY.module, appRoute.INVENTORY.path);
        break;
      }
      case appRoute.REPORTS.module:{
        this.initApp(appRoute.REPORTS.module, appRoute.REPORTS.path);
        break;
      }
      case appRoute.SETTINGS.module:{
        this.initApp(appRoute.SETTINGS.module, appRoute.SETTINGS.path);
        break;
      }
      case appRoute.IMPRINT.module:{
        this.initApp(appRoute.IMPRINT.module, appRoute.IMPRINT.path);
        break;
      }
      default:{

      }
    }
  }

  initApp = (module, url) => {
    this.props.setModule(module);
    this.props.hideSideBar();
    this.redirect(url);
  }

  redirect = (path) => window.location.href = path;

  sideBarToggleClickedHandler = () => {
    this.props.showSideBar();
  }

  render() {
    // Set default username for test purposes
    const user = {username: 'Patricia Kasse', avatar: '/assets/avatars/antonio.svg'};

    return <div>
            <SideBar showSideBar={this.props.sideBarToggle} menuEvent={this.menuEventHandler}/>
              <div className='content'>
                <AppBar title={this.props.module}
                        username={user.username} 
                        avatar={user.avatar} 
                        sideBarToggleClicked={this.sideBarToggleClickedHandler}/>
                <Router>
                  <div>   
                      {/*Show Contacts Page as default  */}
                    <Route path='/' exact component={ContactsContainer}/> 
                    <Route path='/app/contacts' exact component={ContactsContainer}/>
                  </div>
                </Router>
              </div>
          </div>
    
  }
}

const mapStateToProps = state =>{
  return {
     module : state.app.module,
     sideBarToggle: state.app.sideBarToggle
  }
}

const mapDispatchToProps = dispatch =>{
  return {
    setModule : (module) => dispatch(appAction.setModule(module)),
    showSideBar : () => dispatch(appAction.showSideBar()),
    hideSideBar : () => dispatch(appAction.hideSideBar())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
