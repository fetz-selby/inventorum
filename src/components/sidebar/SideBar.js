import React from 'react';
import logo from '../../assets/icons/logo.svg';
import p_o_s from '../../assets/icons/p_o_s.svg';
import invoice from '../../assets/icons/invoice.svg';
import imprint from '../../assets/icons/imprint.svg';
import inventory from '../../assets/icons/inventory.svg';
import reports from '../../assets/icons/reports.svg';
import settings from '../../assets/icons/settings.svg';
import contacts from '../../assets/icons/contacts.svg';
import * as menuAction from '../../store/actions/appRoute';
import PropTypes from 'prop-types';
import '../../assets/styles/layout.css';
import './sidebar.css';

const SideBarLogo = () =>{

    const logo_label = 'inventorum';
    return <ul className='logo-container'>
                <li className='logo'>
                     <img src={logo} alt={logo_label}></img>
                </li>
            </ul>
}

const SideBarMenu = props =>
<ul className='sidebar-list'>
               <li className='sidebar-item' onClick={()=>props.menuClicked(menuAction.POS.module)}>
                   <img src={p_o_s} alt={'point of sales'}/>
                   <div>Point of Sales</div>
               </li>
               <li className='sidebar-item' onClick={()=>props.menuClicked(menuAction.INVOICE_RETURNS.module)}>
                   <img src={invoice} alt={'invoice & returns'}/>
                   <div>Invoice & returns</div>
               </li>
               <li className='sidebar-item' onClick={()=>props.menuClicked(menuAction.CONTACTS.module)}>
                   <img src={contacts} alt={'contacts'}/>
                   <div>Contacts</div>
               </li>
               <li className='sidebar-item' onClick={()=>props.menuClicked(menuAction.INVENTORY.module)}>
                   <img src={inventory} alt={'inventory'}/>
                   <div>Inventory</div>
               </li>
               <li className='sidebar-item' onClick={()=>props.menuClicked(menuAction.REPORTS.module)}>
                   <img src={reports} alt={'reports'}/>
                   <div>Reports</div>
               </li>
               <li className='sidebar-item' onClick={()=>props.menuClicked(menuAction.SETTINGS.module)}>
                   <img src={settings} alt={'settings'}/>
                   <div>Settings</div>
               </li>
               <li className='sidebar-item' onClick={()=>props.menuClicked(menuAction.IMPRINT.module)}>
                   <img src={imprint} alt={'imprint'}/>
                   <div>Imprint</div>
               </li>
            </ul>

const SideBarMenuItemContainer = props =>{
    return <nav className={props.showSideBar ? 'show sidebar-container sidebar' : 'sidebar-container sidebar'}>
            <SideBarLogo />
            <SideBarMenu menuClicked={props.menuEvent}/>
        </nav>
    
}

SideBarMenu.propTypes = {
    menuClicked: PropTypes.func.isRequired
}

SideBarMenuItemContainer.propTypes = {
    showSideBar: PropTypes.bool.isRequired
}

export default SideBarMenuItemContainer

