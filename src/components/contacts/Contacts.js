import React from 'react';
import './contact.css';
import PropTypes from 'prop-types';
import '../../assets/styles/defaults.css';
import Loader from '../loaders/BasicLoader';


const ContactWidget = props =>{
    return <li className={props.inactive?'row-contact inactive-row-contact':'row-contact'}>
        <div className='img-col'>
            <img src={props.contact.avatar} alt={props.contact.name}/>
        </div>
        <div className='col'>
            <div>{props.contact.name}</div>
        </div>
        <div className='col'>
            <div>{props.contact.department}</div>
        </div>
        <div className='col hide-mobile'>
            <div>{props.contact.email}</div>
        </div>
        <div className='col hide-mobile'>
            <div>{props.contact.mobile}</div>
        </div>
        <div className='clearfix'></div>
    </li>
}

const ContactList = props =>
    <ul className={props.contacts.length?'contact-list':'hide'}>
        {props.contacts.map((contact)=>
            <ContactWidget key={contact.id} contact={contact} inactive={props.status}/>
        )}
    </ul>

const InActiveLabel = props =>
        <div className={props.show?'inactive-label inactive':'hide'}>
            Inactive Users
        </div>


const Contacts = props => 
    props.isLoading ? <Loader module={'Contacts'}/>: <div>
    <ContactList contacts={props.activeContacts} status={true}/>
    <InActiveLabel show={props.inactiveContacts.length}/>
    <ContactList contacts={props.inactiveContacts} status={true}/>
    </div>


ContactWidget.propTypes = {
    inactive: PropTypes.bool.isRequired,
    contact: PropTypes.object.isRequired
}

ContactList.propTypes = {
    contacts: PropTypes.array.isRequired
}

InActiveLabel.propTypes = {
    show: PropTypes.number.isRequired
}

Contacts.propTypes = {
    activeContacts: PropTypes.array.isRequired,
    inactiveContacts: PropTypes.array.isRequired,
    isLoading: PropTypes.bool.isRequired
}

export default Contacts;