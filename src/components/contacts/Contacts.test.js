import React from 'react';
import {shallow} from 'enzyme';
import Contacts from './Contacts';

const activeContacts = [{id:1,avatar:'',name:'John Doe',department:'sales',email:'john@gmail.com',mobile:'030164908960', active: true},
                        {id:2,avatar:'',name:'Frank Doe',department:'sales',email:'frank@gmail.com',mobile:'030164908961', active: true},
                        {id:3,avatar:'',name:'Emilly Foo',department:'sales',email:'emilly@gmail.com',mobile:'030164908962', active: true},
                        {id:4,avatar:'',name:'Justin Bar',department:'sales',email:'justin@gmail.com',mobile:'030164908963', active: true}
                    ];
const inactiveContacts = [{id:5,name:'Sam Doe',department:'manager',email:'sam@gmail.com',mobile:'030164908965', active: false}];
const isLoading = false;

const contacts = shallow(<Contacts activeContacts={activeContacts}
                                   inactiveContacts={inactiveContacts}
                                   isLoading={isLoading}/>);


describe('Contacts Component', ()=>{

    it('should have a total children length of activeContacts & inactiveContacts length', ()=>{
       expect(contacts.render().find('.contact-list').children().length).toBe(activeContacts.length+inactiveContacts.length);
    })
})
