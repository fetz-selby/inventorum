import React from 'react';
import './contact.css';
import PropTypes from 'prop-types';

import searchIcon from '../../assets/icons/search.svg';

const ContactSearchInput = props =>
    <div className='contact-search-container'>
        <input className='search-input' ref={props.searchRef} onChange={props.changed}/>
        <div className='clearfix'></div>

        <div className={props.showPlaceholder?'search-placeholder':'hide'}>
            <img className='icon' src={searchIcon} alt=''></img>
            <div className='info' onClick={props.searchTextClicked}>Search User</div>
        </div>
    </div>

ContactSearchInput.propTypes = {
    changed : PropTypes.func.isRequired,
    showPlaceholder: PropTypes.bool.isRequired
}


export default ContactSearchInput;