import * as actions from '../actions/actionTypes';
import * as userActionCreator from '../actions/usersActionCreators';
import {takeLatest, put} from 'redux-saga/effects';
import {delay} from 'redux-saga';
import axios from 'axios';
import {REQUEST_DELAY} from '../../config';
// import {BASE_URL} from '../../config';

function* getAllUsersAsync(){
    // const path = '/api/users?page=1';
    //const users = yield axios.get(BASE_URL+path);
    yield put(userActionCreator.fetchUserLoadingStart());
    const users = yield axios.get('/resources/users.json');
    yield put(userActionCreator.fetchUserFulfilled(users.data));
}

function* searchUser(action){
    yield put(userActionCreator.fetchUserLoadingStart());
    yield delay(REQUEST_DELAY);
    yield put(userActionCreator.searchUserFulfilled(action.payload));
}

export default function* watchUsers(){
    yield takeLatest(actions.FETCH_USERS, getAllUsersAsync);
    yield takeLatest(actions.SEARCH_USER, searchUser)   
}