import {all} from 'redux-saga/effects';
import usersSaga from './userSaga';

export default function* () {
    yield all([
        usersSaga(),
      ])
}