import * as actionTypes from '../actions/actionTypes';
import _ from 'lodash';

const initial = {
    users : [],
    searchValue: '',
    activeUsers : [],
    inactiveUsers : [],
    isLoading: false
}

const reducer = (state = initial, action) => {
    switch(action.type){
        case actionTypes.FETCH_USERS_FULFILLED:{
            const users = [...action.payload];
            const searchValue = '';
            const inactiveUsers = _.filter([...action.payload], {active: false});
            const activeUsers = _.filter([...action.payload], {active: true});
            const isLoading = false;

            return{
                ...state,
                searchValue,
                users,
                inactiveUsers,
                activeUsers,
                isLoading
            }
        }

        case actionTypes.FETCH_USER_LOADING_START:{
            return {
                ...state,
                isLoading: true
            }
        }

        case actionTypes.SEARCH_USER_FULFILLED:{
            const val = action.payload.trim().toLowerCase();

            //Search by all category and search in a uniform case [lowercase]
            const match = _.filter(state.users, (user)=>_.includes(user.email.toLowerCase(), val) || 
                                                              _.includes(user.mobile, val) ||
                                                              _.includes(user.name.toLowerCase(), val) ||
                                                              _.includes(user.department.toLowerCase(), val));
            const activeUsers = _.filter(match, {active: true});
            const inactiveUsers = _.filter(match, {active: false});
            const isLoading = false;

            return{
                ...state,
                activeUsers,
                inactiveUsers,
                isLoading
            }
        }
        default:{
            return{
                ...state
            }
        }
    }
}

export default reducer;