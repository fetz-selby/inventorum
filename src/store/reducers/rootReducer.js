import {combineReducers} from 'redux';
import userReducer from './usersReducer';
import appReducer from './appReducer';

export default combineReducers({
    users : userReducer,
    app : appReducer
})