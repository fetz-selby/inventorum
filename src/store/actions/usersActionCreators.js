import * as actionTypes from './actionTypes';

export const fetchUsers = () => (
    {type: actionTypes.FETCH_USERS}
)

export const fetchUser = (value) => (
    {type: actionTypes.FETCH_USER, payload: value}
)

export const fetchUserLoadingStart = () => (
    {type: actionTypes.FETCH_USER_LOADING_START}
)

export const fetchUserFulfilled = (users) => (
    {type: actionTypes.FETCH_USERS_FULFILLED, payload: users}
)

export const searchUser = (value) => (
    {type: actionTypes.SEARCH_USER, payload: value}
)

export const searchUserFulfilled = (value) => (
    {type: actionTypes.SEARCH_USER_FULFILLED, payload: value}
)