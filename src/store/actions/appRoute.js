export const POS = {module: 'Point of Sale', path: '#/app/pos'};
export const INVOICE_RETURNS = {module:'Invoice and Returns', path: '#/app/invoice'};
export const CONTACTS = {module:'Contacts', path: '#/app/contacts'};
export const INVENTORY = {module:'Inventory', path: '#/app/inventory'};
export const REPORTS = {module:'Reports', path: '#/app/reports'};
export const SETTINGS = {module:'Settings', path: '#/app/settings'};
export const IMPRINT = {module:'Imprint', path: '#/app/imprint'};