import React, { Component } from 'react';
import {connect} from 'react-redux';
import Contacts from '../../components/contacts/Contacts';
import ContactSearchInput from '../../components/contacts/ContactSearchInput';
import * as userActions from '../../store/actions/usersActionCreators';

class ContactsContainer extends Component{

    constructor(){
        super();
        this.state = {
            showPlaceholder:true
        }
        this.showPlaceholder = true;
        this.searchWidget = React.createRef();
    }

    componentDidMount() {
        //Init Users
        this.props.loadUsers();
        
        //Auto focus on search
        this.searchWidget.current.focus();
    }

    inputChangedHandler = (event) =>{
        this.setState({
            showPlaceholder:event.target.value.trim().length?false:true
        })
        this.props.searchUser(event.target.value);
    }
    
    render(){
        return(
            <section>
                <ContactSearchInput searchRef={this.searchWidget} 
                                    changed={this.inputChangedHandler} 
                                    searchTextClicked={()=>this.searchWidget.current.focus()} 
                                    showPlaceholder={this.state.showPlaceholder}>
                </ContactSearchInput>
                <Contacts activeContacts={this.props.activeContacts} 
                          inactiveContacts={this.props.inactiveContacts}
                          isLoading={this.props.isLoading}></Contacts>
            </section>
            );
    }

}

const mapStateToProps = state =>{
    return {
        activeContacts : state.users.activeUsers,
        inactiveContacts : state.users.inactiveUsers,
        isLoading: state.users.isLoading
    }
}

const mapDispatchToProps = dispatch =>{
    return {
        loadUsers : () => dispatch(userActions.fetchUsers()),
        searchUser : (val) => dispatch(userActions.searchUser(val)) 
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ContactsContainer);
